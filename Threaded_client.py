import socket
import threading


class ThreadedClientGetInput(threading.Thread):
    def __init__(self, name):
        threading.Thread.__init__(self)
        self.name = name
        self.port = 12363  # Reserve a port for your service.

    def run(self):
        s = socket.socket()
        s.connect(('localhost', self.port))
        thisreciver = ThreadedClientRecieveData(self.name, s)
        thisreciver.start()
        while True:
            print("client: in while "+str(self.name))
            message_to_be_sent = input()
            s.send(message_to_be_sent.encode('utf-8'))


class ThreadedClientRecieveData(threading.Thread):
    def __init__(self, name, connection):
        threading.Thread.__init__(self)
        self.name = name
        self.connection = connection

    def run(self):
        while True:
            data = self.connection.recv(1024)
            print("client "+str(self.name)+str(data.decode('utf-8')))


client1 = ThreadedClientGetInput("Client-1")
client1.start()












import socket               # Import socket module
import threading


class BroadcastHandler(threading.Thread):
    def __init__(self, name,connection):
        threading.Thread.__init__(self)
        self.name = name
        self.port = 12363  # Reserve a port for your service.
        self.connection = connection

    def run(self):
        while True:
            print("server:in"+str(self.name)+" thread while")
            data = self.connection.recv(1024)
            for cc in clients:
                cc.send(data)


def on_new_client(client_socket, addr):
    while True:
        msg = client_socket.recv(1024)
        client_socket.send(msg)
        client_socket.close()


s = socket.socket()
host = socket.gethostname()
port = 12363
s.bind(('localhost', port))
s.listen(5)
clients = []
connection_number = 0
while True:
        connection_number += 1
        c, address = s.accept()  # Establish connection with client.
        clients.append(c)
        new_client = BroadcastHandler(connection_number, c)
        new_client.start()
        print('Got connection from', address)
